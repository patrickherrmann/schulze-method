# Schulze method

This implementation of the Schulze method takes a list of candidates

```
const candidates = ["A", "B", "C", "D", "E"];
```

and a list of ballots

```
const ballots = [
  [2, 2, 2, 1, 2], // This ballot prefers D, and is otherwise indifferent
  [1, 1, 2, 3, 3], // This ballot prefers A or B. If they cannot win, it prefers C to D or E.
  ...
];
```

and runs an election like so:

```
const schulze = require("schulze-method");
const outcome = schulze.run(candidates, ballots);
console.log(outcome);
// [["E"], ["A"], ["D"], ["C", "B"]]
```

In the output above, E wins, followed by A and then D. C and B are tied in last place.

const expect = require("chai").expect;
const schulze = require("../schulze");

const createBallot = candidates => ranked => {
  return candidates.map(c => ranked.indexOf(c));
};

const createElection = candidatesString => piles => {
  const ballots = [];
  const candidates = candidatesString.split("");
  const cast = createBallot(candidates);
  piles.forEach(([n, str]) => {
    for (let i = 0; i < n; i++) {
      ballots.push(cast(str.split("")));
    }
  });
  return {
    candidates,
    ballots
  };
};

describe("the schulze method", () => {
  describe("Electowiki examples", () => {
    it("should work for example 1", () => {
      const election = createElection("ABCDE")([
        [5, "ACBED"],
        [5, "ADECB"],
        [8, "BEDAC"],
        [3, "CABED"],
        [7, "CAEBD"],
        [2, "CBADE"],
        [7, "DCEBA"],
        [8, "EBADC"]
      ]);
      const outcome = schulze.run(election.candidates.length, election.ballots);
      expect(outcome).to.deep.equal([
        { place: 1, indexes: [4] },
        { place: 2, indexes: [0] },
        { place: 3, indexes: [2] },
        { place: 4, indexes: [1] },
        { place: 5, indexes: [3] }
      ]);
    });

    it("should work for example 2", () => {
      const election = createElection("ABCD")([
        [5, "ACBD"],
        [2, "ACDB"],
        [3, "ADCB"],
        [4, "BACD"],
        [3, "CBDA"],
        [3, "CDBA"],
        [1, "DACB"],
        [5, "DBAC"],
        [4, "DCBA"]
      ]);
      const outcome = schulze.run(election.candidates.length, election.ballots);
      expect(outcome).to.deep.equal([
        { place: 1, indexes: [3] },
        { place: 2, indexes: [0] },
        { place: 3, indexes: [2] },
        { place: 4, indexes: [1] }
      ]);
    });

    it("should work for example 3", () => {
      const election = createElection("ABCDE")([
        [3, "ABDEC"],
        [5, "ADEBC"],
        [1, "ADECB"],
        [2, "BADEC"],
        [2, "BDECA"],
        [4, "CABDE"],
        [6, "CBADE"],
        [2, "DBECA"],
        [5, "DECAB"]
      ]);
      const outcome = schulze.run(election.candidates.length, election.ballots);
      expect(outcome).to.deep.equal([
        { place: 1, indexes: [1] },
        { place: 2, indexes: [0] },
        { place: 3, indexes: [3] },
        { place: 4, indexes: [4] },
        { place: 5, indexes: [2] }
      ]);
    });

    it("should work for example 4", () => {
      const election = createElection("ABCD")([
        [3, "ABCD"],
        [2, "DABC"],
        [2, "DBCA"],
        [2, "CBDA"]
      ]);
      const outcome = schulze.run(election.candidates.length, election.ballots);
      expect(outcome).to.deep.equal([
        { place: 1, indexes: [1, 3] },
        { place: 3, indexes: [0, 2] }
      ]);
    });
  });
});

describe("edge cases", () => {
  it("should work with no candidates", () => {
    const outcome = schulze.run(0, []);
    expect(outcome).to.deep.equal([]);
  });

  it("should work with one candidate", () => {
    const outcome = schulze.run(1, []);
    expect(outcome).to.deep.equal([{ place: 1, indexes: [0] }]);
  });
});

describe("validation", () => {
  it("should reject a malformed candidates array", () => {
    const errors = Array.from(schulze.validate("foo", [[1, 2, 3]]));
    expect(errors).to.have.lengthOf(1);
  });

  it("should reject a malformed ballots array", () => {
    const errors = Array.from(schulze.validate(["A", "B", "C"], "foo"));
    expect(errors).to.have.lengthOf(1);
  });

  it("should reject malformed ballots", () => {
    const errors = Array.from(schulze.validate(["A", "B"], ["foo", 7]));
    expect(errors).to.have.lengthOf(2);
  });

  it("should reject ballots of the wrong length", () => {
    const errors = Array.from(schulze.validate(["A", "B"], [[[0], [1, 2, 3]]]));
    expect(errors).to.have.lengthOf(2);
  });

  it("should reject ballots with non-numeric rankings", () => {
    const errors = Array.from(
      schulze.validate(["A", "B"], [[[0, true], [1, "2"]]])
    );
    expect(errors).to.have.lengthOf(2);
  });

  it("should accept valid arguments", () => {
    const errors = Array.from(schulze.validate(["A", "B"], [[1, 2], [2, 1]]));
    expect(errors).to.have.lengthOf(0);
  });
});

describe("ranks to groups", () => {
  it("should work with no ranks", () => {
    const groups = schulze.ranksToGroups([]);
    expect(groups).to.deep.equal([]);
  });

  it("should work for a small example", () => {
    const groups = schulze.ranksToGroups([3, 1, 2, 1, 2]);
    expect(groups).to.deep.equal([
      { place: 1, indexes: [1, 3] },
      { place: 3, indexes: [2, 4] },
      { place: 5, indexes: [0] }
    ]);
  });

  it("should work for a slightly larger example", () => {
    const groups = schulze.ranksToGroups([5, 5, 5, 4, 4, 3, 3, 2, 1]);
    expect(groups).to.deep.equal([
      { place: 1, indexes: [8] },
      { place: 2, indexes: [7] },
      { place: 3, indexes: [5, 6] },
      { place: 5, indexes: [3, 4] },
      { place: 7, indexes: [0, 1, 2] }
    ]);
  });

  it("should work even with large and negative ranks", () => {
    const groups = schulze.ranksToGroups([99999, -44]);
    expect(groups).to.deep.equal([
      { place: 1, indexes: [1] },
      { place: 2, indexes: [0] }
    ]);
  });

  it("should work with negative ranks", () => {
    const groups = schulze.ranksToGroups([-34, 0, -44]);
    expect(groups).to.deep.equal([
      { place: 1, indexes: [2] },
      { place: 2, indexes: [0] },
      { place: 3, indexes: [1] }
    ]);
  });
});

describe("groups to ranks", () => {
  it("should work with no groups", () => {
    const ranks = schulze.groupsToRanks(0, []);
    expect(ranks).to.deep.equal([]);
  });

  it("should work for a simple example", () => {
    const ranks = schulze.groupsToRanks(4, [[1, 3], [0]]);
    expect(ranks).to.deep.equal([2, 1, 3, 1]);
  });
});
